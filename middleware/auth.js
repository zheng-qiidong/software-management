module.exports = (req,res,next)=>{
    // 有些路由需要放行，及不需要校验session权限，如/login,/logout
    let path = req.path.toLowerCase();
    let unPermissionPath = ['/login','/logout','/dologin','/ajaxlogin','/register','/ajaxregister','/ajaxemail']
    if(!unPermissionPath.includes(path)){
        // 不在放行内则需要校验
        // console.log(req.path,'-session校验')
        // console.log(req.session.userInfoh,'-session校验')

        if(req.session.userInfo){
            // 权限正确
            next();
        }else{
            // 权限失败
            res.redirect('/login');
            return;
        }
    }else{
        // 放行的路由
        next();
    }
     
}