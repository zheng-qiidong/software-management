const { query } = require('express');
const express = require('express');
const router = express.Router();
//导入模型
const queryPromise = require('../model/query-promise.js')
//接口

router.get('/cate', async (req, res) => {
    let sql = `select * from category where is_show = 1`;
    let result = await queryPromise(sql)
    res.json(result)
})

//获取首页分页的文章
router.get('/article', async (req, res) => {
    let { page = 1, pagesize = 3 } = req.query;
    //起始位置 当前页面--乘以每页显示的条数
    let offset = (page - 1) * pagesize;
    let sql = `select t1.*,t2.cate_name from masterpieces1 t1 left join category t2 on t1.cat_id = t2.cate_id 
                where is_delete = 0  limit ${offset}, ${pagesize}`
    // let sql = `select * from masterpieces1 where is_delete = 0 limit ${offset}, ${pagesize}`;
    let result = await queryPromise(sql)
    res.json(result)
})
//获取分类页面文章
router.get('/catelist', async (req, res) => {
    console.log(req.query);
    let { page = 1, pagesize = 2, id } = req.query;
    let offset = (page - 1) * pagesize;
    let sql = `select t1.cate_name,t2.* from category t1 left join masterpieces1 t2 on t1.cate_id = t2.cat_id 
    where t1.cate_id = ${id} and t2.is_delete = 0 limit ${offset},${pagesize}`
    let result = await queryPromise(sql)
    res.json(result)
})

//获取文章详情
router.get('/detail', async (req, res) => {

    let { id } = req.query
    console.log(req.query)
    id = parseInt(id)
    id = isNaN(parseInt(id)) ? 0 : parseInt(id);
    let sql = `select t1.cate_name,t2.* from category t1 left join masterpieces1 t2 on t1.cate_id =t2.cat_id  where t2.id= ${id}`;
    let result = await queryPromise(sql)
    res.json(result[0] || {})
})

//获取分类页面的文章
// router.get('/catelist', async (req, res) => {
//     let { page = 1, pagesize = 2, cat_id } = req.query;
//     let offset = (page - 1) * pagesize;
//     let sql = `selenpm i eslint -D
// })


router.get('*', (req, res) => {
    res.json({ message: "404" })
})


module.exports = router;