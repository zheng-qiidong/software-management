// 连接数据库
const mysql = require('mysql');
const dbConfig = require('../config/dbconfig.js')
const connection = mysql.createConnection(dbConfig);

//连接mysql
connection.connect(function(err){
    if(err){
        throw err;
    }
    console.log('connect mysql success');
});

function dbQuery(sql){
   //1. 返回一个promise 2.成功调用resolve,失败调用reject
   return new Promise((resolve,reject)=>{
       connection.query(sql,(err,result)=>{
           if(err){
                //失败
               reject(err)
           }else{
               // 成功
               resolve(result)
           }
       })
   })
}

// 暴露于模块
module.exports = dbQuery;