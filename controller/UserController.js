
const dbQuery = require('../model/query.js')
let CryptoJS = require('crypto-js')
let { secret } = require('../config/config.js')
let { getNowDate } = require('../util/tool.js')
let UserController = {};

// 输出一个登录表单页面
UserController.login = (req, res) => {
    res.render('login.html')
}

// UserController.dologin = (req,res) => {
//     // 1.接受用户信息
//     let {username,password} = req.body;
//     // 2. 去数据库匹配此用户信息是否正确
//     if(username === 'xiaoquan' && password === '123456'){
//         // 把用户信息存放在session中,session只能存放字符串信息，对象和数组需要转成字符串在存储
//         req.session.userInfo = JSON.stringify({username,password})
//         res.redirect('/')
//     }else{
//         res.redirect('/login')
//     }
//     // 3. 如果正确，把用户信息存储到session中，失败则打回登录页面
// }

// UserController.dologin = (req, res) => {
//     console.log(req.body);
//     // 1.接受用户信息
//     let { username, password } = req.body;
//     password = CryptoJS.MD5(`${password}${secret}`).toString()
//     // 2. 去数据库匹配此用户信息是否正确

//     let sql = `select * from users where username = '${username}' and password = '${password}'`;
//     // 调用模型Model方法操作数据库
//     let data = dbQuery(sql)
//     data.then(rows => {
//         if (rows.length > 0) {
//             // 把用户信息存放在session中,session只能存放字符串信息，对象和数组需要转成字符串在存储
//             let userInfo = rows[0];
//             req.session.userInfo = JSON.stringify(userInfo)
//         }
//         let sql2 = `update users set last_login_date = '${getNowDate()}' where user_id =${userInfo.user_id}`
//         return dbQuery(sql2)
//     }).then(result => {
//         if (result.affectedRows) {
//             res.redirect('/')
//         } else {
//             res.send("<script>alert('用户名或密码错误');location.href='/login'</script>")
//         }
//     }).catch((err) => {
//         if (err) {
//             res.send("<script>alert('用户名或密码错误');location.href='/login'</script>")
//         }
//     })
//     // dbQuery(sql,(err,rows)=>{

//     //         // 更新用户最后的登录时间
//     //         let sql2 = `update users set last_login_date = '${getNowDate()}' where user_id =${userInfo.user_id}`
//     //         dbQuery(sql2,(err,result)=>{
//     //             if(err){  throw err; }
//     //             res.redirect('/')
//     //         })

//     //     }else{
//     //         res.send("<script>alert('用户名或密码错误');location.href='/login'</script>")
//     //         // res.redirect('/login')
//     //     }
//     // })
//     // 3. 如果正确，把用户信息存储到session中，失败则打回登录页面
// }

UserController.ajaxlogin = (req, res) => {
    let { username, password } = req.body;
    password = CryptoJS.MD5(`${password}${secret}`).toString()

    // 2. 去数据库匹配此用户信息是否正确
    let sql = `select * from users where username = '${username}' and password = '${password}'`;
    // 调用模型Model方法操作数据库
    let data = dbQuery(sql)
    data.then(rows => {
        let userInfo = '';
        if (rows.length > 0) {
            // 把用户信息存放在session中,session只能存放字符串信息，对象和数组需要转成字符串在存储
            userInfo = rows[0];
            req.session.userInfo = JSON.stringify(userInfo)
        }
        let sql2 = `update users set last_login_date = '${getNowDate()}' where user_id =${userInfo.user_id}`
        return dbQuery(sql2)
    }).then(result => {
        if (result.affectedRows) {
            res.json({
                message: "success",
                code: 20000
            })
        }
    }).catch((err) => {
        if (err) {
            res.json({
                message: "fail",
                code: -1
            })
        }
    })
    // 1.接受用户信息
    // let { username, password } = req.body;
    // password = CryptoJS.MD5(`${password}${secret}`).toString()
    // // 2. 去数据库匹配此用户信息是否正确

    // let sql = `select * from users where username = '${username}' and password = '${password}'`;
    // // 调用模型Model方法操作数据库
    // dbQuery(sql, (err, rows) => {
    //     if (rows.length > 0) {
    //         // 把用户信息存放在session中,session只能存放字符串信息，对象和数组需要转成字符串在存储
    //         let userInfo = rows[0];
    //         req.session.userInfo = JSON.stringify(userInfo)
    //         // 更新用户最后的登录时间
    //         let sql2 = `update users set last_login_date = '${getNowDate()}' where user_id =${userInfo.user_id}`
    //         dbQuery(sql2, (err, result) => {
    //             if (err) { throw err; }
    //             res.json({
    //                 message: "success",
    //                 code: 20000
    //             })
    //         })

    //     } else {
    //         // res.send("<script>alert('用户名或密码错误');location.href='/login'</script>")
    //         // res.redirect('/login')
    //         res.json({
    //             message: "fail",
    //             code: -1
    //         })
    //     }
    // })
    // 3. 如果正确，把用户信息存储到session中，失败则打回登录页面
}

// 用户退出逻辑
UserController.logout = (req, res) => {
    // 1. 清除用户session信息
    req.session.destroy((err) => {
        if (err) throw err;
    })
    // 2. 重定向到登录页面
    res.redirect('/login')
}

UserController.register = (req, res) => {
    res.render('register.html')
}

UserController.ajaxregister = (req, res) => {
    console.log(req.body);
    // 1.接受参数
    let { username, email, password, repassword } = req.body
    // 2.参数校验
    if ([username, email, password, repassword].includes('')) {
        res.json({
            message: "参数不能为空",
            code: -1
        })
        return;
    }

    if (password !== repassword) {
        res.json({
            message: "两次密码不一致",
            code: -2
        })
        return;
    }
    //3. 入库
    password = CryptoJS.MD5(`${password}${secret}`).toString()
    let last_login_date = getNowDate();
    console.log(typeof last_login_date)
    let sql = `insert into users(username,password,email,last_login_date) value('${username}','${password}','${email}','${last_login_date}')`;

    dbQuery(sql).then(result => {
        if (result.affectedRows) {
            res.json({
                code: 20000,
                message: "success"
            })
        } else {
            res.json({
                code: -3,
                message: "服务器繁忙，请稍后再试"
            })
        }
    })
}

UserController.ajaxemail = (req, res) => {
    let { email } = req.body;
    let sql = `select * from users where email = '${email}'`;
    dbQuery(sql).then(rows => {
        if (rows.length > 0) {
            res.json({
                code: -5,
                message: "邮箱被占用"
            })
        } else {
            res.json({
                code: 20000,
                message: "邮箱可用"
            })
        }
    })
}
//更新密码
UserController.updatePassword = (req, res) => {
    let { oldpwd, newpwd, renewpwd } = req.body;
    console.log(req.body);
    // 1 校验原密码是否一致
    let { user_id } = JSON.parse(req.session.userInfo)
    let sql = `select password from users where user_id = ${user_id}`
    dbQuery(sql).then(result => {
        if (result.length > 0) {
            oldpwd = CryptoJS.MD5(`${oldpwd}${secret}`).toString();
            //         // console.log(oldpwd)
            //         // console.log(result[0].password)
            if (result[0].password !== oldpwd) {
                // 2 原密码输入错误
                res.json({
                    code: -1,
                    message: "原密码输入错误"
                })
            } else {
                //  2 原密码一致才更新新密码
                newpwd = CryptoJS.MD5(`${newpwd}${secret}`).toString();
                let sql = `update users set password = '${newpwd}' where user_id = ${user_id}`
                return dbQuery(sql);
            }
        }
    }).then(result => {
        if (result.affectedRows) {
            res.json({
                code: 20000,
                x: '更新密码成功'
            })
        } else {
            res.json({
                code: -2,
                message: '服务器繁忙 稍后再试'
            })
        }
    })

}
module.exports = UserController;